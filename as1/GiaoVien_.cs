﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_demo
{
     class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien()
        {
        }
        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public new void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
        {
            base.NhapThongTin(hoTen, namSinh, luongCoBan);
        }

        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }
        public new double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }
        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine("He so luong: {0}, Luong: {1}", HeSoLuong, TinhLuong());
        }

        public void XuLy()
        {
            HeSoLuong += 0.6;
        }
    }
}

