﻿using Console_demo;

class Program
{
    static void Main()
    {
        danhSachGiaoVien();
    }



















    public static void danhSachGiaoVien()
    {
        int soLuong = 0;
        bool nhapLai = true;

        while (nhapLai)
        {
            Console.Write("Nhap so luong giao vien: ");
            string input = Console.ReadLine();

            if (int.TryParse(input, out soLuong) && soLuong != 0)
            {
                nhapLai = false;
            }
            else
            {
                Console.WriteLine("So luong khong hop le. Vui long nhap lai.");
            }
        }

        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 0; i < soLuong; i++)
        {
            Console.WriteLine("\nNhap thong tin giao vien {0}", i + 1);
            GiaoVien gv = new GiaoVien();

            bool nhapLaiHoTen = true;
            string hoTen = "";
            while (nhapLaiHoTen)
            {
                Console.Write("Nhap ho ten: ");
                hoTen = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(hoTen))
                {
                    nhapLaiHoTen = false;
                }
                else
                {
                    Console.WriteLine("Ho ten khong hop le. Vui long nhap lai.");
                }
            }

            bool nhapLaiNamSinh = true;
            int namSinh = 0;
            while (nhapLaiNamSinh)
            {
                Console.Write("Nhap nam sinh: ");
                if (int.TryParse(Console.ReadLine(), out namSinh))
                {
                    if ( namSinh < 1900 && namSinh > 2023)
                    {
                        Console.WriteLine("Nam sinh khong hop le. Vui long nhap lai.");
                    }
                    else
                    {
                        nhapLaiNamSinh = false;
                    }
                }
                else
                {
                    Console.WriteLine("Nam sinh khong hop le. Vui long nhap lai.");
                }
            }

            bool nhapLaiLuongCoBan = true;
            double luongCoBan = 0.0;
            while (nhapLaiLuongCoBan)
            {
                Console.Write("Nhap luong co ban: ");
                if (double.TryParse(Console.ReadLine(), out luongCoBan))
                {
                    nhapLaiLuongCoBan = false;
                }
                else
                {
                    Console.WriteLine("Luong co ban khong hop le. Vui long nhap lai.");
                }
            }

            bool nhapLaiHeSoLuong = true;
            double heSoLuong = 0.0;
            while (nhapLaiHeSoLuong)
            {
                Console.Write("Nhap he so luong: ");
                if (double.TryParse(Console.ReadLine(), out heSoLuong))
                {
                    nhapLaiHeSoLuong = false;
                }
                else
                {
                    Console.WriteLine("He so luong khong hop le. Vui long nhap lai.");
                }
            }

            // Tiếp tục xử lý các thông tin khác của giáo viên


            gv.NhapThongTin(hoTen, namSinh, luongCoBan);
            gv.NhapThongTin(heSoLuong);

            danhSachGiaoVien.Add(gv);
        }

        double luongThapNhat = danhSachGiaoVien[0].TinhLuong();
        int viTriLuongThapNhat = 0;

        for (int i = 1; i < danhSachGiaoVien.Count; i++)
        {
            double luong = danhSachGiaoVien[i].TinhLuong();
            if (luong < luongThapNhat)
            {
                luongThapNhat = luong;
                viTriLuongThapNhat = i;
            }
        }

        Console.WriteLine("\nThong tin giao vien co luong thap nhat:");
        danhSachGiaoVien[viTriLuongThapNhat].XuatThongTin();

        Console.ReadLine();
    }
}